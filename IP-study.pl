#!/usr/bin/perl
#
# (c) 2018 https://gitlab.com/mrigf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

use strict;

# no warning for smartmatch usage (used by 
use experimental 'smartmatch';

# debian: libspreadsheet-read-perl libxml-parser-perl geoip-bin geoip-database-contrib     libarchive-zip-perl libxml-twig-perl libfile-slurp-perl libfile-type-perl libimage-size-perl libdomain-publicsuffix-perl dnsutils libchi-perl libarchive-extract-perl libio-compress-perl libnet-ip-perl dnsutils whois

# note: sort by IP block because frequency per IP is often meaning less than
# IP block frequency 

# add local library
use lib "lib"; 

# file lock
use Fcntl qw(:flock);

# logging and console output 
use POSIX qw(strftime);
use Term::ANSIColor qw(:constants);
use Sys::Hostname;

# find IPv4/IPv6 and domains root, sort IPs
use Regexp::Common;
use Domain::PublicSuffix;
use Net::IP;

# easy handling of date output
use Date::Calc qw(Month_to_Text);

# reading spreadsheet
use Spreadsheet::Read;

# writing spreadsheet (remove warnings tied to smartmatch)
# with cosmetics def.
use ODF::lpOD;
{ package ODF::lpOD::Element; sub DESTROY {};  }

# caching results setup
use CHI;
#       expires data in 2 weeks (1209600s)
my $cache_expires_in = "1209600"; 
#       we do not want cache in the working dir, 
#       since we have no clue who has write access here
my $cache_path = "/var/tmp"; 
#       most cache are user specific to avoid write access issues
our $cache_ip_geo = CHI->new(driver => 'File', root_dir => "$cache_path/IP-study.pl-$</cache_ip_geo", expires_in => $cache_expires_in);
our $cache_ip_dns = CHI->new(driver => 'File', root_dir => "$cache_path/IP-study.pl-$</cache_ip_dns", expires_in => $cache_expires_in);
our $cache_ip_whois = CHI->new(driver => 'File', root_dir => "$cache_path/IP-study.pl-$</cache_ip_whois", expires_in => $cache_expires_in);
#       but Tor cache is not, since this script wont write within
our $cache_ip_tor = CHI->new(driver => 'File', root_dir => "$cache_path/IP-study.pl/cache_ip_tor");


############################## FUNCTIONS

sub Debug {
    return;
    print "$_[0]\n";
}

sub Color {    
    local $Term::ANSIColor::AUTORESET = 1;
    return CYAN $_[0];
}

sub ColorError {    
    local $Term::ANSIColor::AUTORESET = 1;
    return RED $_[0];
}

sub GeoIP {
    # search geoip database
    my $ip = $_[0];

    # check earlier cached results
    my $cached = $cache_ip_geo->get($ip);
    # equal to 0 mean done already and no result, so return empty
    return if $cached eq 0;
    # otherwise return result as it is
    return $cached if $cached;

    # otherwise do the actual search 
    my $bin = "geoiplookup";
    my $result;
    $bin = "geoiplookup6" if $ip =~ m/($RE{net}{IPv6})/g;
    open(GEOIP, "$bin $ip|");
    Debug("$bin $ip", 2);
    while(<GEOIP>) {
	chomp();
	next if /IP Address not found/;
	if (/GeoIP Country Edition\:/ or /GeoIP Country V6 Edition\:/)  {
	    # first line, keep only the country name
	    # skip if no "," within (means an error)
	    my $index = rindex($_,",");
	    $result .= substr($_,$index+1);
	}
	if (/GeoIP City Edition/ or /GeoIP City V6 Edition/) {
	    s/^.*\://g;  # remove everything before :
	    # first line, keep only the country name
	    # skip if no "," within (means an error)
	    my @data = split('\,', $_);
	    # 0 = US
	    # 1 = CA
	    # 2 = California
	    # 3 = Mountain View
	    # 4 = 94303
	    # 5 + 6 = GPS coord
	    @data[2] =~ s/^\s+//; # strip white space from the beginning
	    @data[2] =~ s/\s+$//; # strip white space from the end
	    @data[2] =~ s/^N\/A$/?/; # replace N/A by ?	
	    
	    @data[3] =~ s/^\s+//; # strip white space from the beginning
	    @data[3] =~ s/\s+$//; # strip white space from the end
	    @data[3] =~ s/^N\/A$/?/; # replace N/A by ?
	    
	    
	    @data[5] =~ s/^\s+//; # strip white space from the beginning
	    @data[5] =~ s/\s+$//; # strip white space from the end

	    @data[6] =~ s/^\s+//; # strip white space from the beginning
	    @data[6] =~ s/\s+$//; # strip white space from the ends/;//;

	    $result .= "/".@data[2]."/".@data[3]." ".@data[5]." ".@data[6]." ";
	}
	if (/GeoIP ASNum Edition\:/ or /GeoIP ASNum V6 Edition\:/) {
	    # asnum, keep only the owner, remove the asnum)
	    s/^.*\: AS[0-9]* //g;  # remove everything before :
	    s/^\s+//; # strip white space from the beginning
	    s/\s+$//; # strip white space from the end
	    $result .= " ($_)";
	}
    }
    close(GEOIP); 
    $result =~ s/^\s+//; # strip white space from the beginning
    $result =~ s/\s+$//; # strip white space from the end

    # no result at this point? set cache to 0 to avoid doing this search further
    $result = 0 unless $result;
    
    # cache result
    $cache_ip_geo->set($ip, $result);

    # and finally returns empty if no result 
    return if $result eq 0;
    # or proper value
    return $result;
}

our $ip_dns_publicsuffix =  Domain::PublicSuffix->new({'data_file' => '/usr/share/publicsuffix/effective_tld_names.dat'});
sub DnsIP {
    # search DNS records
    my $ip = $_[0];

    # check earlier cached results
    my $cached = $cache_ip_dns->get($ip);
    # equal to 0 mean done already and no result, so return empty
    return if $cached eq 0;
    # otherwise return result as it is
    return $cached if $cached;

    # otherwise do the actual search 
    my $bin = "dig +short -x";
    my $rdns;
    my $result;
    open(DNSIP, "$bin $ip|");
    Debug("$bin $ip", 2);
    while(<DNSIP>) {
	chomp();s/\. ?$//sg;
	$rdns .= $_;
    }
    close(DNSIP);

    # no result at this point? set cache to 0 to avoid doing this search further
    $result = 0 unless $rdns;
    
    # otherwise find out the domain
    if ($rdns) {
	my $rdns_domain = $ip_dns_publicsuffix->get_root_domain($rdns) 
	    or print $ip_dns_publicsuffix->error()." was $rdns\n";
	my $rdns_without_domain = $rdns;
	$rdns_without_domain =~ s/$rdns_domain$//igx;
	$result = "$rdns_domain";
	$result .= " ($rdns_without_domain)" if $rdns_without_domain;

    }
	
    # cache result
    $cache_ip_dns->set($ip, $result);

    # and finally returns empty if no result 
    return if $result eq 0;
    # or proper value
    return $result;
    
}

sub WhoisIP {
    # search whois (RIPE/ARIN/etc) records
    my $ip = $_[0];
    
    # check earlier cached results
    my $cached = $cache_ip_whois->get($ip);
    # equal to 0 mean done already and no result, so return empty
    return if $cached eq 0;
    # otherwise return result as it is
    return $cached if $cached;    

    # otherwise do the actual search 
    my $bin = "whois";
    my ($result, $country);
    open(WHOISIP, "$bin $ip|");
    Debug("$bin $ip", 2);
    while(<WHOISIP>) {
	$result .= "$1 " if /^netname\:\s*(.*)$/i;
	$result .= "$1 " if /^descr\:\s*(.*)$/i;
	$result .= "$1 " if /^nettype\:\s*(.*)$/i;
	$result .= "$1 " if /^owner\:\s*(.*)$/i;
	$country = "[$1] " if /^country\:\s*(.*)$/i;
	# if we reach a "role" enty, we passed the inetnum general info, skip
	last if /^role:\s/i;
	# same if we hit an empty line and we already got some data
	last if /^$/ and $result;
    }
    close(WHOISIP);

    # no result at this point? set cache to 0 to avoid doing this search further
    $result = 0 unless $result;

    # include the country in the results
    $result = $country.$result if $country;
    
    # cache result
    $cache_ip_whois->set($ip, $result);

    # and finally returns empty if no result 
    return if $result eq 0;
    # or proper value
    return $result;
}

sub TorIP {
    # search Tor local database
    # (cache must be up to date!)
    my $result = $cache_ip_tor->get($_[0]);

    # return possible if there is any result and unless ARG2 is true
    return "possible" if $result and !$_[1];

    # otherwise return untouched results
   # return $result;

    ####
    
    # otherwise prepare a easily readable list:
    # to make so we actually need to build a hash
    my %ip;
    for (split(",", $result)) {
	$ip{$_} = $_;
    }

    # empty $result to be reused with ordered content
    $result = "";

    # localized month name (primitive approach)
    my %month_name = (
	"01" => "janvier",
	"02" => "février",
	"03" => "mars",
	"04" => "avril",
	"05" => "mai",
	"06" => "juin",
	"07" => "juillet",
	"08" => "août",
	"09" => "septembre",
	"10" => "octobre",
	"11" => "novembre",
	"12" => "décembre");
    
    # now, try to check if we are periods of time of usage, to group them
    for (sort(keys %ip)){
	# skip if it was unset along the way
	next unless $ip{$_};

	my ($start_year, $start_month) = split("/", $_);
	my ($end_year, $end_month) = ($start_year, $start_month);
	my ($next_year, $next_month) = ($start_year, $start_month);
	# loop over possible valid month
	while ($ip{"$next_year/$next_month"}) {
	    # record ending value
	    ($end_year, $end_month) = ($next_year, $next_month);
	    # unset so we wont work with this value further
	    delete($ip{"$next_year/$next_month"});
	    # increment a month
	    ($next_year, $next_month, undef) = Date::Calc::Add_Delta_YM($next_year,$next_month,1,0,1);
	    # month must be written with a 0 before, like everywhere else
	    $next_month = sprintf("%02d", $next_month);	    
	}
	
	if ($start_year eq $end_year and $start_month eq $end_month) {
	    # single value
	    $result .= $month_name{$start_month}." $start_year,";
	} elsif ($start_year eq $end_year) {
	    # period/range, over the same year
	    $result .= "de ".$month_name{$start_month}." à ".$month_name{$end_month}." $end_year,"
	} else {
	    # period/range, over multiple years
	    $result .= "de ".$month_name{$start_month}." $start_year à ".$month_name{$end_month}." $end_year,"
	}
    }
    
    return $result;    
}

############################## RUN

# silently forbid concurrent runs
# (return error, so chained commands stops)
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit(1);

# loop though all spreadsheets provided
foreach my $spreadsheet_source (glob("*.csv *.xlsx *.ods")) {
    # skip if starting with ~$, since MS Office add files named like this when
    # some XLS are being edited
    print ColorError("ignore $spreadsheet_source")." (backup MS Office)\n" and next
	if $spreadsheet_source =~ /^\~\$/;

    # skip if already handled
    print ColorError("ignore $spreadsheet_source")." (déjà un résultat produit)\n" and next 
	if -e $spreadsheet_source."+IPgeoloc.csv";
    print ColorError("ignore $spreadsheet_source")." (déjà un résultat produit)\n" and next 
	if -e $spreadsheet_source."+IPstudy.ods";
    
    # skip if likely result of a previous run
    print ColorError("ignore $spreadsheet_source")." (généré par $0)\n" and next 
	if $spreadsheet_source =~ /\+IPgeoloc.csv$/;
    print ColorError("ignore $spreadsheet_source")." (généré par $0)\n" and next 
	if $spreadsheet_source =~ /\+IPstudy.ods$/;

    # check if the file was last modified at least X seconds ago (hopefully it means
    # it is fully copied)
    my $ctime = (stat($spreadsheet_source))[10];
    print ColorError("ignore $spreadsheet_source")." (dernière modification ".(time() - $ctime)."s seulement)\n" and next    
	if (time() - $ctime) < 30;
    
    # slurp data with relevant module
    my $file = ReadData($spreadsheet_source);
    Debug("< $spreadsheet_source");

    # prepare new file
    my $spreadsheet_final = odf_document->create('spreadsheet');
    my $spreadsheet_final_context = $spreadsheet_final->get_body;
    $spreadsheet_final_context->clear;

    # cosmetics (cannot be set outside of the loop 
    # otherwise error "Element already belonging to a tree"
    my $odf_style_title = odf_style->create('table cell',
					    name                => 'Titles',
					    background_color    => 'beige',
					    border_bottom       => 'thin solid #404040'
	);
    $odf_style_title->set_properties(
	area    => 'paragraph',
	align   => 'center'
	);
    $odf_style_title->set_properties(
	area      => 'text',
	weight    => 'bold',
	);
    $spreadsheet_final->register_style($odf_style_title);
    my $odf_style_title_orig = odf_style->create('table cell',
						 name                => 'Titles original col',
						 background_color    => 'gray60',
						 border_bottom       => 'thin solid #404040'
	);    
    $odf_style_title_orig->set_properties(
	area    => 'paragraph',
	align   => 'center'
	);
    $odf_style_title_orig->set_properties(
	area      => 'text',
	weight    => 'bold',
	);
    $spreadsheet_final->register_style($odf_style_title_orig);
    
    my $odf_style_neutral_alt = odf_style->create('table cell',
						  name                => 'Reset style alt',	
						  background_color    => 'gray95',
	);
    
    $spreadsheet_final->register_style($odf_style_neutral_alt);


    
    
    # store rows, since want to be able to restore data as it is
    my %file_row;
   
    # store noticed IP, with number of occurrences 
    my %file_ip;

    # ties IP to rows, assuming there could be only one IP per row (otherwise it
    # makes no sense)
    
    print Color("Lecture de $spreadsheet_source:")."\n";

    # study each sheet
    for (my $sheet = 1; $sheet <= $file->[0]{"sheets"}; $sheet++) {

	# prepare new sheet: "Détail sheet will contain as many row as the original
	# plus (ip found|domain (reversedns)|geoip|whois IP|tor IP) placed first
	my $spreadsheet_final_sheet_detail = odf_table->create($file->[$sheet]{"label"},
							       width => ($file->[$sheet]{"maxcol"}+5), 
							       length => $file->[$sheet]{"maxrow"}+1);
	
	print "Feuillet $sheet ($file->[$sheet]{maxrow} x $file->[$sheet]{maxcol})  ...\n";
	Debug("> $spreadsheet_source $sheet");


	# add new first line with titles
	$spreadsheet_final_sheet_detail->get_cell(0,0)->set_text("Adresse IP");
	$spreadsheet_final_sheet_detail->get_cell(0,0)->set_style($odf_style_title);
	$spreadsheet_final_sheet_detail->get_cell(0,1)->set_text("DNS inversé");
	$spreadsheet_final_sheet_detail->get_cell(0,1)->set_style($odf_style_title);
	$spreadsheet_final_sheet_detail->get_cell(0,2)->set_text("Géo IP");
	$spreadsheet_final_sheet_detail->get_cell(0,2)->set_style($odf_style_title);
	$spreadsheet_final_sheet_detail->get_cell(0,3)->set_text("Données AS");
	$spreadsheet_final_sheet_detail->get_cell(0,3)->set_style($odf_style_title);
	$spreadsheet_final_sheet_detail->get_cell(0,4)->set_text("Tor");
	$spreadsheet_final_sheet_detail->get_cell(0,4)->set_style($odf_style_title);
	$spreadsheet_final_sheet_detail->get_cell(0,5)->set_text($file->[$sheet]{"label"});
	$spreadsheet_final_sheet_detail->get_cell(0,5)->set_span(columns => $file->[$sheet]{"maxcol"});
	$spreadsheet_final_sheet_detail->get_cell(0,5)->set_style($odf_style_title_orig);
	
	
	# check every possible line (from minrow to maxrow)
	foreach my $row (1 ... $file->[$sheet]{"maxrow"}) {
	    my $row_ip;
	    print $row;

	    # check every possible cell (from mincol to maxcol
	    foreach my $col (1 ... $file->[$sheet]{"maxcol"}) {
		
		# IPv4 collection
		while ($file->[$sheet]{cell}[$col][$row] =~ m/($RE{net}{IPv4})/g) {
		    $file_ip{$1} += 1;
		    $row_ip = $1;
		}

		# IPv6 collection
		while ($file->[$sheet]{cell}[$col][$row] =~ m/($RE{net}{IPv6})/g) { 
		    $file_ip{$1} += 1;
		    $row_ip = $1;
		}
		
		# save line content as it is moved +5 to the right
		# (actually, +4 since cell A1 = 0,0)
		# even if the field only contain IP, might be duplicate but we want to
		# keep the original line that might comes with comments or useful
		# info (Received mail header, for instance)
		$spreadsheet_final_sheet_detail->get_cell(($row),($col+4))->set_text($file->[$sheet]{cell}[$col][$row]);
		
		print ".";
	    }

	    # row is over, identify any IP found and set data accordingly in first fields
	
	    # ip is first field
	    $spreadsheet_final_sheet_detail->get_cell(($row),0)->set_text($row_ip) if $row_ip;
	    # reverse DNS 
	    $spreadsheet_final_sheet_detail->get_cell(($row),1)->set_text(DnsIP($row_ip)) if $row_ip;
	    
	    # geoip  
	    $spreadsheet_final_sheet_detail->get_cell(($row),2)->set_text(GeoIP($row_ip)) if $row_ip;
	    
	    # whois RIPE
	    $spreadsheet_final_sheet_detail->get_cell(($row),3)->set_text(WhoisIP($row_ip)) if $row_ip;

	    # known Tor
	    $spreadsheet_final_sheet_detail->get_cell(($row),4)->set_text(TorIP($row_ip)) if $row_ip;
	    
	    print "/";
	}

	# actually store new sheet in the new spreadsheet
	$spreadsheet_final_context->insert_element($spreadsheet_final_sheet_detail);
	    
	print "\n";
	
    }


    ###  SYNTHESE
    # build a short list with all IP in the document
    #  ip|occurrences|rdns|geoip
    my $spreadsheet_final_sheet_short = odf_table->create("Synthèse", 
							  width => 6, 
							  length => (keys(%file_ip)+1));
    print "Feuillet synthèse (".(keys(%file_ip)+1)." x 6)...\n";
    Debug("> $spreadsheet_source synthèse");

    my $row = 0;
    $spreadsheet_final_sheet_short->get_cell($row,0)->set_text("Adresse IP");
    $spreadsheet_final_sheet_short->get_cell($row,0)->set_style($odf_style_title);
    $spreadsheet_final_sheet_short->get_cell($row,1)->set_text("Occurrences");
    $spreadsheet_final_sheet_short->get_cell($row,1)->set_style($odf_style_title);
    $spreadsheet_final_sheet_short->get_cell($row,2)->set_text("DNS inversé");
    $spreadsheet_final_sheet_short->get_cell($row,2)->set_style($odf_style_title);
    $spreadsheet_final_sheet_short->get_cell($row,3)->set_text("Géo IP");
    $spreadsheet_final_sheet_short->get_cell($row,3)->set_style($odf_style_title);
    $spreadsheet_final_sheet_short->get_cell($row,4)->set_text("Données AS");
    $spreadsheet_final_sheet_short->get_cell($row,4)->set_style($odf_style_title);
    $spreadsheet_final_sheet_short->get_cell($row,5)->set_text("Tor");
    $spreadsheet_final_sheet_short->get_cell($row,5)->set_style($odf_style_title);

    # build a list of sorted IPs
    # https://stackoverflow.com/questions/6917314/how-can-i-sort-a-list-of-ip-addresses-in-perl
    my @sorted_ip = map  { $_->[0] } sort { $a->[1] <=> $b->[1] } map  { [ $_, eval { Net::IP->new( $_ )->intip } ] }  keys %file_ip;
    
    # loop around sorted IP
    my %file_ip_tor;
    foreach my $ip (@sorted_ip) {
	$row++;
	$spreadsheet_final_sheet_short->get_cell($row,0)->set_text($ip);
	$spreadsheet_final_sheet_short->get_cell($row,1)->set_text($file_ip{$ip});
	$spreadsheet_final_sheet_short->get_cell($row,2)->set_text(DnsIP($ip));
	$spreadsheet_final_sheet_short->get_cell($row,3)->set_text(GeoIP($ip));
	$spreadsheet_final_sheet_short->get_cell($row,4)->set_text(WhoisIP($ip));
	$spreadsheet_final_sheet_short->get_cell($row,5)->set_text(TorIP($ip));
	$file_ip_tor{$ip} = 1 if TorIP($ip); # store list of know Tor nodes
	
    }    
    $spreadsheet_final_context->insert_element($spreadsheet_final_sheet_short);


    ###  TOR NODES
    # build a list with all Tor nodes in the document
    # (only if at least one node was found)
    if (keys(%file_ip_tor)) {
	my $spreadsheet_final_sheet_tor = odf_table->create("Nœuds Tor", 
							    width => 2, 
							    length => 1);
	print "Feuillet nœuds Tor (".(keys(%file_ip_tor)+1)."+? x 2)...\n";
	Debug("> $spreadsheet_source tor");
	
	$row = 0;
	$spreadsheet_final_sheet_tor->get_cell($row,0)->set_text("Adresse IP");
	$spreadsheet_final_sheet_tor->get_cell($row,0)->set_style($odf_style_title);
	$spreadsheet_final_sheet_tor->get_cell($row,1)->set_text("Participation à Tor (mois)");
	$spreadsheet_final_sheet_tor->get_cell($row,1)->set_style($odf_style_title);

	# sort by IPs
	my @sorted_ip_tor = map  { $_->[0] } sort { $a->[1] <=> $b->[1] } map  { [ $_, eval { Net::IP->new( $_ )->intip } ] }  keys %file_ip_tor;
	
	my $ip_style = 0;
	foreach my $ip (@sorted_ip_tor) {
	    # change style for each IP
	    $ip_style++;
	    # record the starting row for this ip
	    my $ip_row = ($row+1);
	    
	    # and then, for each IP, get the list of possible months
	    # (the cache contains list separated per ",")
	    my (@month) = (split(",", TorIP($ip, 1)));
    	    foreach my $month (@month) {
		$row++;
		$spreadsheet_final_sheet_tor->add_row;

		$spreadsheet_final_sheet_tor->get_cell($row,0)->set_text($ip);
		$spreadsheet_final_sheet_tor->get_cell($row,1)->set_text($month);

		# style style per ip
		unless ($ip_style % 2 == 0) {
		    $spreadsheet_final_sheet_tor->get_cell($row,0)->set_style();	
		    $spreadsheet_final_sheet_tor->get_cell($row,1)->set_style();
		} else {
		    $spreadsheet_final_sheet_tor->get_cell($row,0)->set_style($odf_style_neutral_alt);
		    $spreadsheet_final_sheet_tor->get_cell($row,1)->set_style($odf_style_neutral_alt);
		}
		
	    }

	    
	}
	
	$spreadsheet_final_context->insert_element($spreadsheet_final_sheet_tor,
						   after => $spreadsheet_final_sheet_short);

	
    }

    # Save file 
    Debug("> $spreadsheet_source+IPstudy.ods sauvegarde");
    $spreadsheet_final->save(target => $spreadsheet_source."+IPstudy.ods");
       
}



# EOF
