#!/usr/bin/perl
#
# (c) 2018 https://gitlab.com/mrigf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

use strict;

my $collector_baseurl = "https://collector.torproject.org/archive/exit-lists/exit-list-";
my $collector_endeurl = ".tar.xz";

# file lock
use Fcntl qw(:flock);

# file download/handling
use LWP::Simple; 
use File::Temp qw(tempfile tempdir);
use File::Path qw(remove_tree);
use Archive::Extract;

# date handling
use Date::Calc qw(Today Localtime);

# find IPv4/IPv6 
use Regexp::Common;

# will create a cache of tor nodes to be used by IP-study
# (should be run as a daily cronjob)
use CHI;
# this script only will write in this cache, but IP-study will read it
our $cache = CHI->new(driver => 'File', root_dir => "/var/tmp/IP-study.pl/cache_ip_tor");

# we will store
# + every IP possible, with a sublist of month it was found
#     IP = date1,date2,date3
# + a marker to know that we imported data for the given day

# silently forbid concurrent runs
# (return error, so chained commands stops)
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit(1);


# First find out which data we need to acquire:
# loop every month from 2010-02-22 (first exit-list package available 
# on https://collector.torproject.org/archive/exit-lists/ ) until now-24h
# and check if we miss any marker

my ($year, $month, $day) = (2012, 2, 21);
my ($end_year, $end_month, $end_day, undef, undef, undef, undef, undef, undef)  = Localtime((time()-86400));
my %temp_archive;

while (($year < $end_year) || 
       ($year == $end_year && $month < $end_month) ||
       ($year == $end_year && $month <= $end_month && $day < $end_day)
       
    ) {
    # increment a day
    ($year, $month, $day) = Date::Calc::Add_Delta_YMD($year,$month,$day,0,0,1);
        
    # skip if marker exists for this date
    next if $cache->get("$year/".sprintf("%02d/%02d", $month, $day));

    # needs to be a bit verbose: at least indicate whenever it needs to obtain data
    # for a day
    print "$year/".sprintf("%02d/%02d", $month, $day);

    # check if the monthly archive was already downloaded in a temporary dir
    my $monthly_dir = $temp_archive{"$year/$month"};
    unless (-d $monthly_dir) {
	
	# if not, download the relevant monthly file in temporary file
	# skip if HTTP return code > 3 (error 400/500)
	my (undef, $monthly_archive) = tempfile(UNLINK => 1, OPEN => 0, SUFFIX => $collector_endeurl);
	my $monthly_url = "$collector_baseurl$year-".sprintf("%02d", $month)."$collector_endeurl";
	my $get = getstore($monthly_url, $monthly_archive);
	print "Unable to download $monthly_url to $monthly_archive ($get), skipping.\n" and next if (substr($get, 0, 1) > 3);

	# uncompress in temporary directory
	# (skip on failure to extract)
	($monthly_dir) = tempdir(UNLINK => 1);
	my $extractor = Archive::Extract->new(archive => $monthly_archive);
	$extractor->extract(to => $monthly_dir)
	    or print "Unable to extract $monthly_archive to $monthly_dir, skipping.\n" and next;
	unlink($monthly_archive); # forcibly remove downloaded file after extraction

	# mark it so we wont download it twice
	$temp_archive{"$year/$month"} = $monthly_dir;
    }

    # at this point, we simply have to import every files within the temporary directory
    # for this day

    # skip if still no folder for this day
    my $daily_dir = $monthly_dir."/exit-list-$year-".sprintf("%02d", $month)."/".sprintf("%02d", $day);
    print "missing $daily_dir\n" and next unless -d $daily_dir;
    
    # otherwise read every file within and grab IPs
    foreach my $daily_file (glob("$daily_dir/*")) {
	# skip if not a file
	next unless -f $daily_file;
	# skip if a symlink
	next if -l $daily_file;

	# read every file, slurp every IP
	open(DAILY_FILE, "< $daily_file");
	while(<DAILY_FILE>) {
	    # IPv4 + IPv6 collection
	    while ($_ =~ m/($RE{net}{IPv4}|$RE{net}{IPv6})/g) {
		# check if this IP is known
		# if not, register it with current $year/$month as value for easy
		# further chronological sorting
		unless ($cache->get($1)) {
		    $cache->set($1, "$year/".sprintf("%02d", $month));	
		    next;
		}

		# otherwise add the current month only if not yet listed
		my $needs_update = 1;
		for (split(",", $cache->get($1))) {
		    if ($_ eq "$year/".sprintf("%02d", $month)) {
			# unflag update requirements if found
			$needs_update = 0;
			last;
		    }
		}

		# update the cache only if required
		next unless $needs_update;
		$cache->set($1, $cache->get($1).",$year/".sprintf("%02d", $month));
	    }
	}
	close(DAILY_FILE);
	
    }
	
    # add marker for this date so we skip it from now on
    $cache->set("$year/".sprintf("%02d/%02d", $month, $day), 1); 

    # needs to be a bit verbose: at least indicate whenever it needs to obtain data
    # for a day
    print " OK. ";

   
}

# cleanup: UNLINK=1 failed to be always honored
foreach my $dir (values %temp_archive) {
    die("$dir is a symlink instead of a directory, possible tampering!") if -l $dir;
    die("$dir should be a directory, possible tampering") unless -d $dir; 
    remove_tree($dir)
	or print "failed to remove $dir\n";
}
